#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"library.h"
#include"date.h"

/*
void date_tester()
 {
	date_t d;
     date_accept(&d);
     date_print(&d);
	date_t d1 = { 20,2,2010 };
	date_t d2 = { 2,1,2010 };

    date_t r = date_add(d, 366);
	int dif=date_cmp(d1, d2);
	printf("difference= %d\n", dif);
}
*/

void tester()
{
    user_t u;
	book_t b;
	payment_t p;
	issuerecord_t r;
	bookcopy_t c;
	user_accept(&u);
	user_display(&u);
	book_accept(&b);
	book_display(&b);
	bookcopy_accept(&c);
    bookcopy_display(&c);	
    issuerecord_accept(&r);
	issuerecord_display(&r);
	payment_accept(&p);
	payment_display(&p);  
}


void sign_in()
{
    // find the user in the users file.
	// check input password is correct.
	// if correct, call user_area() based on its role.
    char email[30], password[10];
	user_t u;
	int invalid_user = 0;
	// input email and password from user.
	printf("email: ");
	scanf("%s", email);
	printf("password: ");
	scanf("%s", password);
	// find the user in the users file by email.
	if(user_find_by_email(&u, email) == 1) {
		// check input password is correct.
		if(strcmp(password, u.password) == 0) {
			// special case: check for owner login
			if(strcmp(email, EMAIL_OWNER) == 0)
				strcpy(u.role, ROLE_OWNER);

			// if correct, call user_area() based on its role.
			if(strcmp(u.role, ROLE_OWNER) == 0)
				owner_area(&u);
			else if(strcmp(u.role, ROLE_LIBRARIAN) == 0)
				librarian_area(&u);
			else if(strcmp(u.role, ROLE_MEMBER) == 0)
				member_area(&u);
			else
				invalid_user = 1;
		}
		else
			invalid_user = 1;
	}
	else
		invalid_user = 1;

	if(invalid_user)
		printf("Invalid email, password or role.\n");
}


void sign_up()
{
   user_t u;
   user_accept(&u);
	// write user details into the users file.
   user_add(&u);

}


int main()
{ 
    printf("Hello library!!\n");
    //date_tester();
    //tester();
   
    int choice;
	do 
	{
		printf("\n\n0.Exit\n1. Sign In\n2. Sign Up\nEnter choice: ");
		scanf("%d", &choice);
		switch (choice)
		{
		case 1:                                  
			    sign_in();                      // Sign In
			     break;
		case 2:	     
		         sign_up();	                    // Sign Up
			     break;
		}
	}while(choice != 0);


      return 0;
}

